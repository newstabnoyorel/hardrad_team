
import pandas as pd
from pandas import ExcelWriter
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import sklearn as sk

from sklearn.preprocessing import LabelEncoder

# This file can be called after calling analiz_dop_uslug.py
intake_per_serv = pd.read_excel('intake_per_serv_sorted.xlsx')


df_suvk = pd.read_csv('ch.xackaton_suvk_yur_platnie_nachisleniya_i_oplati(1).csv')

df_contracts = pd.read_csv('ch.xackaton_po_yur_dannie_po_dogovoru.csv', usecols=['id', 'finance_source_name', 'msfo_group_name',
       'okved_name', 'voltage', 'user_subgroup','contract_type'])

##
# Определение id содержащихся в сувк и id в контрактах с учётом неоднократности
##

def le_coder(dframe):

    col_names = dframe.columns
    output = []
    for names in col_names[1:]:
        label_encoder = LabelEncoder()
        temp_arr = label_encoder.fit_transform(dframe[names])
        output.append(temp_arr)

    return np.array(output)

#arr = le_coder(df_contracts)
#arr = np.transpose(arr)

id_arr = df_suvk['id'].unique() # ids of suvk

##
# Следует взять ай-ди. Выбрать его вхождение в группу. Далее, проверить, были ли заключены контракты ниже позиции 70 этим id.
#
##

def check_rare_service(df_suvk, id_arr, rare_service,coeff):
    # dframe - suvk po uslugam
    # id_arr - ай-ди компаний, которые есть в базе услуг СУВК
    # rare_service - датафрейм с упорядоченными по относительной кумулятивной сумме услугами,
    # где параметром coeff задаётся с какой услуги ведётся отсчёт
    output_id = []
    output_merchant_type = []
    rare_service = rare_service['Unnamed: 0'].loc[coeff:]
    for ids in id_arr:

        sub_serv_title = df_suvk.loc[df_suvk['id'] == ids]['service_name'] # купленные услуги по уникальному айди

        #print(sub_serv_title)
        log_sub_sum = []
        for servs in sub_serv_title:                                     # выбор по купленным услугам
            logic_sum = sum(rare_service.str.contains(servs,regex=False))
            log_sub_sum.append(logic_sum)
        output_id.append(ids)
        output_merchant_type.append(sum(log_sub_sum))
        #print(ids,sum(log_sub_sum))

    #out = output_id, output_merchant_type

    #df_data = pd.DataFrame(data=[np.array(output_id).reshape(-1,1), np.array(output_merchant_type).reshape(-1,1)])
    return output_id, output_merchant_type



output_id, output_merchant_type = check_rare_service(df_suvk, id_arr, intake_per_serv, coeff=70)
np_output_id = np.array(output_id).reshape(-1,1)
np_output_merchant_type = np.array(output_merchant_type) >= 1
np_output_merchant_type = np_output_merchant_type.reshape(-1, 1).astype(float)

# Массив с ай-ди компаний, которые хотя бы раз закупали нерелевантные услуги
out = np.concatenate((np_output_id,np_output_merchant_type), axis=1)

contracts_who_buy_rare_serv = df_contracts.loc[df_contracts['id'].isin(id_arr)]

# https://stackoverflow.com/questions/36004976/count-frequency-of-values-in-pandas-dataframe-column
print(contracts_who_buy_rare_serv['user_subgroup'].value_counts())

print(len(df_contracts['id'].unique()))


























