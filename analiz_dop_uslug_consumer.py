
import pandas as pd
from pandas import ExcelWriter
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

df = pd.read_csv('ch.xackaton_suvk_bitovie_abon_platnie_nachisleniya_i_oplati.csv')
print(df.head())
# define unique names
unique_serv_name = df['service_name'].unique()
cost_df = df['services_sum_cost_with_nds']

def replacer(sub_df):
    new_arr = np.array(sub_df).reshape(-1,1)
    for i,digit in enumerate(new_arr):
        temp_dig = digit[0].replace(',','.')
        new_arr[i] = float(np.array(temp_dig))
    return new_arr

def intake_per_services(all_df,unique_serv_name):
  output = []
  for serv_name in unique_serv_name:
    #print(serv_name)
    temp = replacer(all_df.loc[all_df['service_name'] == serv_name]['services_sum_cost_with_nds'])
    output.append(sum(temp))
  return output

sum_cost = sum(replacer(cost_df))
print(sum_cost)
int_per_serv = np.array(intake_per_services(df,unique_serv_name)).reshape(-1,1)
df_out = pd.DataFrame(data=int_per_serv, index=unique_serv_name)
df_out.columns = ['intake_per_service']
df_sorted_out = df_out.sort_values(by=['intake_per_service'], ascending=False)

cumsum_df_out = df_sorted_out.cumsum()/sum_cost

with ExcelWriter("intake_per_serv_sorted_cons.xlsx") as writer:

   cumsum_df_out.to_excel(writer)


fig = plt.figure(figsize=(12,8))
plt.plot(np.array(cumsum_df_out['intake_per_service']))
plt.xticks(fontsize=18)
plt.yticks(fontsize=18)
plt.xlabel('Индексы видов услуг', fontsize=15)
plt.ylabel('Нормированная кумулятивная сумма выручки от продажи услуг, отн.ед.', fontsize=14)
plt.gca().set(xlim=(0, 805), ylim=(0, 1))
plt.title('Анализ услуг по предоставленной витрине',fontsize=16)
plt.savefig('service_analysis_cons.png')




